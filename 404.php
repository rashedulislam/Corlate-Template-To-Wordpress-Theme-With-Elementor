<?php get_header('2'); ?>

<section class="window-height" id="error" style="background-image: url(<?php echo get_template_directory_uri().'/assets/images/404.png'; ?>)">
       <div class="container">
            <h1>404</h1>
            <p>Oops! Something is wrong</p>
            <a class="btn btn-primary" href="<?php echo site_url()?>">Back to home</a>
       </div>
    </section>

<?php get_footer('2'); ?>