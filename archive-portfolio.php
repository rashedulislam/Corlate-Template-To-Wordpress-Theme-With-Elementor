<?php get_header();?>

<div class="page-title" style="background-image: url(<?php echo get_template_directory_uri().'/assets/images/page-title.png'; ?>)">
        <h1>Portfolio</h1>
</div>
    
    <section id="portfolio">
        <div class="container">
            <div class="center">
                <h2>Recent work</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
            </div>

            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default active" href="#" data-filter="*">All Works</a></li>
                <?php 
                    $terms = get_terms( 'portfolio_category', array(
                        'hide_empty' => false,
                    ) );
                    foreach ($terms as $term) { ?>
                        <li><a class="btn btn-default" href="#" data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li> 
                    <?php } ?>
            </ul>
            <!--/#portfolio-filter-->

            <div class="row">
                <div class="portfolio-items">

                <?php 

                    $args = array(
                        'post_type' => 'portfolio',
                        'showposts' => -1,
                    );
                    // the query
                    $the_query = new WP_Query( $args ); ?>
                    
                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

                    <div class="portfolio-item <?php 
                        $term_lists = wp_get_post_terms($post->ID, 'portfolio_category');
                        foreach ($term_lists as $term):
                            echo $term->slug.' ';
                        endforeach; ?> col-xs-12 col-sm-4 col-md-3 single-work">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url();?>" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <a class="preview" href="<?php echo get_the_permalink();?>"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </section>
    <!--/#portfolio-item-->

<?php get_footer();