<?php global $corlate; ?>

<section id="bottom">
        <div class="container fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-2">
					<?php dynamic_sidebar('footer_one')?>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
								
								<?php dynamic_sidebar('footer_two')?>
                    
                        </div>
                        <!--/.col-md-3-->

                        <div class="col-md-3 col-sm-6">

								<?php dynamic_sidebar('footer_three'); ?>

                        </div>
                        <!--/.col-md-3-->

                        <div class="col-md-3 col-sm-6">

								<?php dynamic_sidebar('footer_four'); ?>

                        </div>
                        <!--/.col-md-3-->

                        <div class="col-md-3 col-sm-6">

								<?php dynamic_sidebar('footer_five'); ?>

                        </div>
                        <!--/.col-md-3-->
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!--/#bottom-->

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
				<?php echo $corlate['footer_copyright']?>
                </div>
                <div class="col-sm-6">

					<?php 
						wp_nav_menu( array(
							'theme_location'  => 'corlate_footer_navigation',
							'menu_class'      => 'pull-right',
							'fallback_cb'     => false,
						) );
					?>
                    <!-- <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </footer>
	<!--/#footer-->
	
	<?php wp_footer();?>
</body>
</html>
