<?php

function register_navwalker(){
	require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );


if ( ! function_exists( 'corlate_setup' ) ) :

	function corlate_setup() {

		load_theme_textdomain( 'corlate', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'corlate_header_navigation' => esc_html__( 'Corlate Header Nav Menu', 'corlate' ),
			'corlate_footer_navigation' => esc_html__( 'Corlate Footer Nav Menu', 'corlate' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'corlate_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'corlate_setup' );

function corlate_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'corlate_content_width', 640 );
}
add_action( 'after_setup_theme', 'corlate_content_width', 0 );


function corlate_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'corlate' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'corlate' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Area One', 'corlate' ),
		'id'            => 'footer_one',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Area Two', 'corlate' ),
		'id'            => 'footer_two',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Area Three', 'corlate' ),
		'id'            => 'footer_three',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Area Four', 'corlate' ),
		'id'            => 'footer_four',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Area Five', 'corlate' ),
		'id'            => 'footer_five',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'corlate_widgets_init' );


function corlate_scripts() {

	wp_enqueue_style( 'corlate_bootstrap_min', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style( 'corlate_font_awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
	wp_enqueue_style( 'corlate_animate_min', get_template_directory_uri() . '/assets/css/animate.min.css');
	wp_enqueue_style( 'corlate_prettyPhoto', get_template_directory_uri() . '/assets/css/prettyPhoto.css');
	wp_enqueue_style( 'corlate_owl_carousel_min', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');
	wp_enqueue_style( 'corlate_icomoon', get_template_directory_uri() . '/assets/css/icomoon.css');
	wp_enqueue_style( 'corlate_main', get_template_directory_uri() . '/assets/css/main.css');
	wp_enqueue_style( 'corlate_responsive', get_template_directory_uri() . '/assets/css/responsive.css');
	wp_enqueue_style( 'corlate_main_style', get_stylesheet_uri() );


	wp_enqueue_script( 'corlate_jquery', get_template_directory_uri() . '/assets/js/jquery.js', array('jquery'), '', false );
	wp_enqueue_script( 'corlate_bootstrap_min', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'corlate_prettyPhoto_jquery', get_template_directory_uri() . '/assets/js/jquery.prettyPhoto.js', array('jquery'), '', true );
	wp_enqueue_script( 'corlate_jowl_carousel_min', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'corlate_jquery_isotope_min', get_template_directory_uri() . '/assets/js/jquery.isotope.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'corlate_main_js', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'corlate_scripts' );


require get_template_directory() . '/inc/custom-header.php';

require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/template-functions.php';

require get_template_directory() . '/inc/customizer.php';

if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function new_excerpt_more($more) {
 return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


function posts_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'posts_excerpt_length');


require_once get_template_directory().'/inc/redux-framework-master/redux-framework.php';
require_once get_template_directory().'/inc/corlate-theme-options.php';



function corlate_custom_portfolio() {
    $labels = array(
        'name'                  => _x( 'Portfolio', 'corlate' ),
        'singular_name'         => _x( 'Portfolio', 'corlate' ),
        'menu_name'             => _x( 'Portfolio', 'corlate' ),
        'name_admin_bar'        => _x( 'Portfolio', 'corlate' ),
        'add_new'               => __( 'Add New Portfolio', 'corlate' ),
        'add_new_item'          => __( 'Add New Portfolio', 'corlate' ),
        'new_item'              => __( 'New Portfolio', 'corlate' ),
        'edit_item'             => __( 'Edit Portfolio', 'corlate' ),
        'view_item'             => __( 'View Portfolio', 'corlate' ),
        'all_items'             => __( 'All Portfolio', 'corlate' ),
        'search_items'          => __( 'Search Portfolios', 'corlate' ),
        'parent_item_colon'     => __( 'Parent Portfolios:', 'corlate' ),
        'not_found'             => __( 'No Portfolios found.', 'corlate' ),
        'not_found_in_trash'    => __( 'No Portfolios found in Trash.', 'corlate' ),
        'featured_image'        => _x( 'Portfolio Cover Image',  'corlate' ),
        'set_featured_image'    => _x( 'Set Portfolio image', 'corlate' ),
        'remove_featured_image' => _x( 'Remove Portfolio image', 'corlate' ),
        'use_featured_image'    => _x( 'Use as Portfolio image', 'corlate' ),
        'archives'              => _x( 'Portfolio archives', 'corlate' ),
        'insert_into_item'      => _x( 'Insert into Portfolio', 'corlate' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Portfolio', 'corlate' ),
        'filter_items_list'     => _x( 'Filter Portfolios list', 'corlate' ),
        'items_list_navigation' => _x( 'Portfolios list navigation', 'corlate' ),
        'items_list'            => _x( 'Portfolios list', 'corlate' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true, 
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
		//'taxonomies'         => array('category', 'post_tag'),
		'exclude_from_search'=> false
    );
 
    register_post_type( 'portfolio', $args );
}
 
add_action( 'init', 'corlate_custom_portfolio' );


function create_portfolio_taxonomies() {

	$labels = array(
		'name'              => _x( 'Portfolio Categories', 'corlate' ),
		'singular_name'     => _x( 'Portfolio Category', 'corlate' ),
		'search_items'      => __( 'Search Portfolio Category', 'corlate' ),
		'all_items'         => __( 'All Portfolio Category', 'corlate' ),
		'parent_item'       => __( 'Parent Portfolio Category', 'corlate' ),
		'parent_item_colon' => __( 'Parent Portfolio Category:', 'corlate' ),
		'edit_item'         => __( 'Edit Portfolio Category', 'corlate' ),
		'update_item'       => __( 'Update Portfolio Category', 'corlate' ),
		'add_new_item'      => __( 'Add New Portfolio Category', 'corlate' ),
		'new_item_name'     => __( 'New Portfolio Category Name', 'corlate' ),
		'menu_name'         => __( 'Portfolio Category', 'corlate' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'portfolio_category' ),
	);

	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

	// // Add new taxonomy, NOT hierarchical (like tags)
	// $labels = array(
	// 	'name'                       => _x( 'Writers', 'taxonomy general name', 'textdomain' ),
	// 	'singular_name'              => _x( 'Writer', 'taxonomy singular name', 'textdomain' ),
	// 	'search_items'               => __( 'Search Writers', 'textdomain' ),
	// 	'popular_items'              => __( 'Popular Writers', 'textdomain' ),
	// 	'all_items'                  => __( 'All Writers', 'textdomain' ),
	// 	'parent_item'                => null,
	// 	'parent_item_colon'          => null,
	// 	'edit_item'                  => __( 'Edit Writer', 'textdomain' ),
	// 	'update_item'                => __( 'Update Writer', 'textdomain' ),
	// 	'add_new_item'               => __( 'Add New Writer', 'textdomain' ),
	// 	'new_item_name'              => __( 'New Writer Name', 'textdomain' ),
	// 	'separate_items_with_commas' => __( 'Separate writers with commas', 'textdomain' ),
	// 	'add_or_remove_items'        => __( 'Add or remove writers', 'textdomain' ),
	// 	'choose_from_most_used'      => __( 'Choose from the most used writers', 'textdomain' ),
	// 	'not_found'                  => __( 'No writers found.', 'textdomain' ),
	// 	'menu_name'                  => __( 'Writers', 'textdomain' ),
	// );

	// $args = array(
	// 	'hierarchical'          => false,
	// 	'labels'                => $labels,
	// 	'show_ui'               => true,
	// 	'show_admin_column'     => true,
	// 	'update_count_callback' => '_update_post_term_count',
	// 	'query_var'             => true,
	// 	'rewrite'               => array( 'slug' => 'writer' ),
	// );

	// register_taxonomy( 'writer', 'book', $args );
}

add_action( 'init', 'create_portfolio_taxonomies');




