<?php global $corlate; ?>
<!DOCTYPE html>
<html <?php language_attributes();?>>  
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>" />
	
	<!-- Place favicon.ico in the root directory -->

	<link rel="shortcut icon" href="<?php echo get_theme_file_uri('assets/images/ico/favicon.ico') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-144-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-114-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-72-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-57-precomposed.png') ?>">
	
	<?php wp_head();?>
 </head>
 <body class="homepage" <?php body_class(); ?>>

 