<?php global $corlate; ?>
<!DOCTYPE html>
<html <?php language_attributes();?>>  
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>" />
	
	<!-- Place favicon.ico in the root directory -->

	<link rel="shortcut icon" href="<?php echo get_theme_file_uri('assets/images/ico/favicon.ico') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-144-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-114-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-72-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_theme_file_uri('assets/images/ico/apple-touch-icon-57-precomposed.png') ?>">
	
	<?php wp_head();?>
 </head>
 <body class="homepage" <?php body_class(); ?>>

 <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="top-number">
                            <p><i class="fa fa-phone-square"></i><?php echo $corlate['header_contact']?></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="social">
                            <ul class="social-share">

							<?php if (isset($corlate['facebook_link']) && !empty($corlate['facebook_link'])) { ?>

							<li><a href="<?php echo $corlate['facebook_link']; ?>"><i class="fa fa-facebook"></i></a></li> 

							<?php }  if (isset($corlate['twitter_link']) && !empty($corlate['twitter_link'])) { ?>
							<li><a href="<?php echo $corlate['twitter_link']; ?>"><i class="fa fa-twitter"></i></a></li>

							<?php }  if (isset($corlate['linkedin_link']) && !empty($corlate['linkedin_link'])) { ?>

							<li><a href="<?php echo $corlate['linkedin_link']; ?>"><i class="fa fa-linkedin"></i></a></li>

							<?php } if (isset($corlate['dribbble_link']) && !empty($corlate['dribbble_link'])) { ?>

							<li><a href="<?php echo $corlate['dribbble_link']; ?>"><i class="fa fa-dribbble"></i></a></li>
							
							<?php } if (isset($corlate['skype_link']) && !empty($corlate['skype_link'])) { ?>
								
							<li><a href="<?php echo $corlate['skype_link']; ?>"><i class="fa fa-skype"></i></a></li> <?php } ?>

                            </ul>
                            <div class="search">
                                <form role="form" action="/">
                                    <input type="text" class="search-form" name="s" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo $corlate['header_logo']['url']?>" alt="logo"></a>
				</div>
				
				<?php
					wp_nav_menu( array(
						'theme_location'    => 'corlate_header_navigation',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse navbar-right',
						'menu_class'        => 'nav navbar-nav',
						'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
						'walker'            => new WP_Bootstrap_Navwalker(),
					) );
				?>
                
            </div>
            <!--/.container-->
        </nav>
        <!--/nav-->

    </header>