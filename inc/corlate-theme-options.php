<?php

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    $opt_name = "corlate";

    $theme = wp_get_theme();

    $args = array(

        'opt_name'             => $opt_name,

        'display_name'         => $theme->get( 'Name' ),

        'display_version'      => $theme->get( 'Version' ),

        'menu_type'            => 'menu',

        'allow_sub_menu'       => true,

        'menu_title'           => __( 'Corlate Options', 'corlate' ),
        'page_title'           => __( 'Corlate Options', 'corlate' ),

        'google_api_key'       => '',

        'google_update_weekly' => false,

        'async_typography'     => true,

        'admin_bar'            => false,

        'admin_bar_icon'       => 'dashicons-portfolio',

        'admin_bar_priority'   => 50,

        'global_variable'      => '',

        'dev_mode'             => false,

        'update_notice'        => false,

        'customizer'           => true,

        'page_priority'        => 90,

        'page_parent'          => 'themes.php',

        'page_permissions'     => 'manage_options',

        'menu_icon'            => '',

        'last_tab'             => '',

        'page_icon'            => 'icon-themes',

        'page_slug'            => 'corlate_options',

        'save_defaults'        => true,

        'default_show'         => false,

        'default_mark'         => '',

        'show_import_export'   => false,

        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,

        'output_tag'           => true,

        'database'             => '',

        'use_cdn'              => true,

        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'corlate' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'corlate' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'corlate' ),
    );

    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'corlate' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'corlate' );
    }

    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'corlate' );

    Redux::setArgs( $opt_name, $args );

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'corlate' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'corlate' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'corlate' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'corlate' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'corlate' );
    Redux::setHelpSidebar( $opt_name, $content );




    // -> START Basic Fields


    Redux::setSection( $opt_name, array(
        'title'  => __( 'Corlate Header Options', 'corlate' ),
        'id'     => 'corlate_header',
        'desc'   => __( 'Corlate Header Section', 'corlate' ),
        'icon'   => 'el el-home',
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Contact Area', 'corlate' ),
        'id'         => 'header_contact_section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'header_contact',
                'type'     => 'text',
                'title'    => __( 'Contact', 'corlate' ),
                'desc'     => __( 'Enter Your Contact Here', 'corlate' ),
                'subtitle' => __( 'Your phone number', 'corlate' ),
                'default'  => '+0123 456 70 90',
            )
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header Logo Area', 'corlate' ),
        'id'         => 'header_logo_area',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'header_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Website Header Logo', 'corlate' ),
                'desc'     => __( 'Please upload your website header logo here', 'corlate' ),
                'default'  => array(
                    'url'=> get_template_directory_uri(). '/assets/images/logo.png'
                ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header Social Links', 'corlate' ),
        'id'     => 'header_social',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'           => 'facebook_link',
                'type'         => 'text',
                'title'        => __('Facebook Link', 'corlate'),
                'default'      => '#'
            ),
            array(
                'id'           => 'twitter_link',
                'type'         => 'text',
                'title'        => __('Twitter Link', 'corlate'),
                'default'      => '#'
            ),
            array(
                'id'           => 'linkedin_link',
                'type'         => 'text',
                'title'        => __('Linkedin Link', 'corlate'),
                'default'      => '#'
            ),
            array(
                'id'           => 'dribbble_link',
                'type'         => 'text',
                'title'        => __('Dribbble Link', 'corlate'),
                'default'      => '#'
            ),
            array(
                'id'           => 'skype_link',
                'type'         => 'text',
                'title'        => __('Skype Link', 'corlate'),
                'default'      => '#'
            ),
            
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'  => __( 'corlate Footer Options', 'corlate' ),
        'id'     => 'corlate_footer',
        'desc'   => __( 'corlate Footer Section', 'corlate' ),
        'icon'   => 'el el-star-alt',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer Logo', 'corlate' ),
        'desc'       => __( 'Footer Logo Field', 'corlate' ),
        'id'         => 'footer_logo_area',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'footer_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Website footer Logo', 'corlate' ),
                'subtitle' => __( 'website footer logo field', 'corlate' ),
                'desc'     => __( 'Please upload your website footer logo here', 'corlate' ),
                'default'  => array(
                    'url'=> get_template_directory_uri(). '/assets/images/logo-black.png'
                ),
            ),
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer Copyright', 'corlate' ),
        'desc'       => __( 'Footer Copyright Description Section', 'corlate' ),
        'id'         => 'footer_copyright_area',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'footer_copyright',
                'type'     => 'editor',
                'title'    => __( 'Copyright', 'corlate' ),
                'desc'     => __( 'Enter Copyright Description Here', 'corlate' ),
                'default'  => '© 2019 ShapeBootstrap. All Rights Reserved.',
                'args'     => array(
                    'wpautop' => false,
                )
            ),
        )
    ) );


    /*
     * <--- END SECTIONS
     */

