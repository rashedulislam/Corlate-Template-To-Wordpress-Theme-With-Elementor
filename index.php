<?php /* Template Name: Blog Template */ 
   get_header();
?>

    <div class="page-title" style="background-image: url(<?php echo get_template_directory_uri().'/assets/images/page-title.png'; ?>)">
        <h1>Blog</h1>
    </div>
    
    <section id="blog">

        <div class="blog container">
            <div class="row">
                <div class="col-md-8">

                 
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="blog-item">
                    <a href="<?php echo get_the_permalink();?>">
				<?php if(has_post_thumbnail()){ ?>
				<img class="img-responsive img-blog" src="<?php echo get_the_post_thumbnail_url();?>" width="100%" alt="" /><?php  } ?> </a> 
                        <div class="blog-content">
                            <a class="blog_cat"><?php the_category(', ')?></a>
                            <h2><a href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></h2>
                            <h3><?php echo get_the_excerpt(); ?></h3>
                            <a class="readmore" href="<?php echo get_the_permalink();?>">Read More <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <?php endwhile; 
                    wp_reset_postdata(); 
                  // the_posts_pagination();
                 else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>

                    <!--/.blog-item-->         
                </div>
                <!--/.col-md-8-->

                <aside class="col-md-4">
                    <?php dynamic_sidebar('sidebar-1'); ?>
                </aside>
            </div>
            <!--/.row-->
            <div class="row">
                <div class="col-md-12 text-center">

                <?php the_posts_pagination(); ?>

                </div>
            </div>
        </div>
    </section>
    <!--/#blog-->

<?php get_footer();