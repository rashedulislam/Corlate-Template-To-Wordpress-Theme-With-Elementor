<?php get_header(); ?>

    <div class="page-title" style="background-image: url(<?php echo get_template_directory_uri().'/assets/images/page-title.png'; ?>)">
        <h1>Single Portfolio</h1>
    </div>


    <section id="blog">

        <div class="blog container">
            <div class="row">
                <div class="col-md-12">

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="blog-item">
                        <a href="<?php echo get_the_permalink();?>"><img class="img-responsive img-blog" src="<?php echo get_the_post_thumbnail_url();?>" width="100%" alt="" /></a>
                        <div class="blog-content">
                        <?php 
                        $term_lists = wp_get_post_terms($post->ID, 'portfolio_category');
                        foreach ($term_lists as $term): ?>
                            <a href="<?php echo get_term_link($term);?>" class="blog_cat"><?php echo $term->name.' &nbsp;';?></a>
                       <?php endforeach; ?>
                            
                            <h2><a href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></h2>
                            <p>Starting Date: <i class="fa fa-clock-o"></i> <?php echo get_field('project_start_date');?> &nbsp;  Ending Date: <i class="fa fa-clock-o"></i> <?php echo get_field('project_end_date');?> &nbsp; Customer: <i class="fa fa-user"></i> <?php echo get_field('customer_name');?></p>
                            <?php echo get_the_content(); ?>
                        </div>
                    </div>
                <?php endwhile; else : ?>
                    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>

                </div>
                <!--/.col-md-8-->
            </div>
            <!--/.row-->
        </div>
    </section>
    <!--/#blog-->

<?php get_footer();