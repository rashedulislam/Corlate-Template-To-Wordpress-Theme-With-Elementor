<?php get_header(); ?>

    <div class="page-title" style="background-image: url(<?php echo get_template_directory_uri().'/assets/images/page-title.png'; ?>)">
        <h1>Single Blog</h1>
    </div>


    <section id="blog">

        <div class="blog container">
            <div class="row">
                <div class="col-md-8">

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="blog-item">
                        <a href="<?php echo get_the_permalink();?>"><img class="img-responsive img-blog" src="<?php echo get_the_post_thumbnail_url();?>" width="100%" alt="" /></a>
                        <div class="blog-content">
                            <a class="blog_cat"><?php the_category(', ')?></a>
                            <h2><?php echo get_the_title();?></h2>
                            <div class="post-meta">
                                <p>By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>"><?php echo get_the_author();?></a></p>
                                <p><i class="fa fa-clock-o"></i> <a href="#"><?php echo get_the_date() ; ?></a></p> 
                                <p><i class="fa fa-comment"></i> <a href="#"><?php echo  get_comments_number(); ?></a></p>
                            </div>
                            <h3><?php echo get_the_content(); ?></h3>
                            
                            <div class="inner-meta">
                                <ul class="tags">
                                    <?php $post_tags = get_the_tags();
                                    foreach ($post_tags as $tag) { ?>
                                        <li><a href="<?php echo get_tag_link($tag->term_id) ; ?>"><?php echo $tag->name ; ?></a></li> 
                                  <?php } ?>
                                </ul>
                            </div>
                            
                            <div class="comments">
                              <?php  if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif; ?>
                            </div>
                     </div>
                </div>
                <?php endwhile; else : ?>
                    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>

                    <!--/.blog-item-->


                </div>
                <!--/.col-md-8-->

                <aside class="col-md-4">
                     <?php dynamic_sidebar('sidebar-1'); ?>
                </aside>
            </div>
            <!--/.row-->
        </div>
    </section>
    <!--/#blog-->

<?php get_footer();